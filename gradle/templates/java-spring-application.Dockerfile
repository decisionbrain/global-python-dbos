# Copyright (c) DecisionBrain SAS. All Rights Reserved.
#
# This Dockerfile is used for all Docker images containing a Java Spring application (except if a Dockerfile is
# provided in the project).

# Use an AdoptOpenJDK 11 JRE as the base
# See https://hub.docker.com/_/adoptopenjdk
FROM index.docker.io/library/adoptopenjdk:11-jre-hotspot

# Add the application JAR
COPY *.jar /app.jar

# Force the Tomcat port to 8080. Or to the value of environment variable 'SERVER_PORT' if the docker-compose.yml sets
# one. (The port is defined in application.yml by property 'server.port' and is overridden by setting this environment
# variable according to Spring Boot's convention -- see https://docs.spring.io/spring-boot/docs/current/reference/html/spring-boot-features.html#boot-features-external-config.)
ENV SERVER_PORT 8080

# Expose the port. This is just documentation (see https://stackoverflow.com/a/56826180/3711784).
EXPOSE $SERVER_PORT

# Run the application
ENTRYPOINT [ "java", "-jar", "/app.jar" ]

### Notes
#
# - There is no need to 'touch' the JAR file. The Spring Docker guide (https://spring.io/guides/gs/spring-boot-docker/)
#   once recommended to include the following statement: RUN bash -c 'touch app.jar'. This is no longer the case, and
#   is explained at https://github.com/jhipster/generator-jhipster/issues/4670.
#
# - If you want to add JVM options (such as memory sizing), set the 'JAVA_TOOL_OPTIONS' environment variable in your
#   docker-compose.yml file (or OKD configuration, etc.). The 'JAVA_OPTS' or '_JAVA_OPTS' variables have never been
#   officially supported, whereas the 'JAVA_TOOL_OPTIONS' variable is.
#   See https://docs.oracle.com/javase/7/docs/webnotes/tsg/TSG-VM/html/envvars.html#gbmsy.
#
# - To add arguments to the Java application, append them to the command line. In a docker-compose.yml file
#   this is achieved by adding 'command: [ arg1, arg2, ... ]' in the section for the application under 'services:'.
#
# - To add or override a Spring property (say you want to set 'foo.bar' to 'hux'), there are several options. The
#   simplest is to add the command line argument '--foo.bar=hux' to the Java application. Which in a docker-compose.yml
#   file is achieved with 'command: [ "--foo.bar=hux" ]'.
