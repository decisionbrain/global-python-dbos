#!/bin/env python
#
# This Python script is a sample Python task that can be run by the Optimization Server.
#
# It takes a CSV collector (zip archive from DBGene) as input and outputs a new CSV collector with some new GeneIssues.
# This task is declared in worker.yml configuration file and also in DBGene's tasks.
#
# This task take advantages of the Python libraries that come the Optimization Server and DBGene.
#


from pandas import DataFrame

from optimserver.workerbridge import ExecutionContext

import com.glob.optimization.engine as engine
import sys

from dbgene.data import CsvCollectorArchive, DataFrameDict, DataChecker


###
# Init the worker bridge (ExecutionContext)
execution_context = ExecutionContext()
execution_context.start()
def should_stop_callback():
    print("Stop callback has been called")
    sys.exit(1)
    # To do : output last result and stop the processing
execution_context.add_should_stop_callback(should_stop_callback)


print("Reading input...")
input_collector_path = execution_context.get_input_path("inputCollector")


print("Processing...")
# If you need to emit metrics, you have to pass the execution_context to your engine.
data_frame_dict = engine.run(input_collector_path)

print("Writing output...")
output_path = execution_context.get_output_path("outputCollector")
output_archive = CsvCollectorArchive(output_path, "w")
data_frame_dict.store_collector_archive(output_archive)


# Stop the ExecutionContext properly
execution_context.stop()
