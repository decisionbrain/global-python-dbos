This module contains the Python engine task that can be executed on the Optimization Server.

The workers documentation can be found here : https://documentation-dbos-integration.okd.decisionbrain.io/workers/

If you need add a Python module as dependency, simply add it in the `:processing:python-engine` module (see the README file). 

You can call the engine manually through the worker's task using `gradle :workers:python-engine-worker:pythonRun`.

Use `gradle :workers:python-engine-worker:tasks` for more help.
