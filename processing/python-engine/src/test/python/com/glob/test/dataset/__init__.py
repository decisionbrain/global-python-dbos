
import os.path


def get_archive_path(file_name):
    return os.path.dirname(os.path.realpath(__file__))+("/%s.zip"%file_name)
