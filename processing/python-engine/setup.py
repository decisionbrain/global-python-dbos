import setuptools

setuptools.setup(
    name="global-optimization-engine",
    version="1.0.11",  # Updated automatically by updateCode Gradle task, do not edit
    packages=['com.glob.optimization.engine'],
    package_dir={"": "src/main/python"},
    python_requires='>=3.6',
    install_requires=[  # Updated automatically by Gradle task ensureInstallRequires, do not edit
        'dbgene-data==4.0.1.3',
        'optimserver-workerbridge==3.4.1',
        'pandas==1.1.0',
        'numpy==1.19.3',
        'docplex==2.19.*'
    ],
)
