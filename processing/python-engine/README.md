This module contains the Python engine that do the optimization computations.

If you need add a Python module as dependency, simply add it with a `pip` dependency in the `build.gradle` file.

You can update the version in the `setup.py` file using the `updateCode` task.
The consistency between `setup.py` and `build.gradle` is checked automatically, the build fails if `setup.py` needs an update.

You can run the task from the command line to test it and test the engine on a particular set of data using the `pythonRun` task.

Use `gradle :workers:python-engine:tasks` for more help.
