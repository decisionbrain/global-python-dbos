
from pandas import DataFrame

from dbgene.data import CsvCollectorArchive, DataFrameDict, DataChecker
from dbgene.data.constants import INTERNAL_ID_FIELD
from dbgene.data.utils import new_internal_id
from dbgene.data.utils import datetime_to_timestamp

from datetime import datetime
from datetime import timedelta
import ast
from .gpo_model import GPO_Model

# from docplex.cp.model import CpoModel, CpoStepFunction, INTERVAL_MIN, INTERVAL_MAX
# from gpo_model import GPO_Model

SOLUTION = "Solution"

# If you need to emit metrics, you have to add a parameter of type ExecutionContext (optimserver.workerbridge module) and use its methods.
def run(archive_path: str) -> DataFrameDict:
    input_data_frame_dict: DataFrame = DataFrameDict()
    input_data_frame_dict.load_collector_archive(CsvCollectorArchive(archive_path))

    # run the engine
    engine_result_dict= run_engine(input_data_frame_dict)

    # finalise data output preparation
    data_frame_dict= prepare_output(engine_result_dict)

    # validate data output
    validate_output(data_frame_dict)

    return data_frame_dict

def validate_output( data_frame_dict : DataFrameDict ) :
    return None


def prepare_output( engine_result_dict: dict ) -> DataFrameDict:
    # Solution
    sol_df = engine_result_dict[SOLUTION]

    print("Preparing output...")
    data_frame_dict: DataFrame = DataFrameDict()
    data_frame_dict[SOLUTION] = sol_df

    return data_frame_dict


def run_engine(input_data_frame_dict: DataFrameDict) -> dict:
    input_scenario = input_data_frame_dict['scenario-5']
    selected_scenario = input_scenario.to_dict(orient='records')[0]

    con = ast.literal_eval(selected_scenario['constraint_weights'])
    selected_scenario['constraint_weights'] = con
    print(selected_scenario)
    solve_method = 1 # 1 - Regular, 2 - Lexicographic, 3 - Lexicographic (v2), 4 - Relaxer

    # building model
    mdl2 = GPO_Model(input_data_frame_dict,name='Stevos',scenario=selected_scenario, solve_method=solve_method)
    mdl2.build_model()

    # solving model - when solve_method = 3, switching to bespoke lexicographic function
    if not solve_method == 3:
        mdl2.solve_model()
    else:
        mdl2.lexo_v2_solve()
    #dd-cell

    #dd-cell
    # printing the slack variables and their respective coefficient
    if mdl2.inc_slack:
        print(mdl2.slack_df)
    #dd-cell
    df = mdl2.slack_df
    #dd-cell
    df = mdl2.slack_df
    for i in df['variable_names']:
        print(type(i))
    #dd-cell
    # displaying the selected panels in a dataframe
    inc_ls = []
    for i in mdl2.bin_var_dic:
        if mdl2.bin_var_dic[i].solution_value == 1:
            inc_ls.append(i)

    res_df = mdl2.df_panel.loc[inc_ls]
    # MEI moved later
#     outputs['solution'] = res_df
    #dd-cell
    print('\033[1m','Solution checks:', '\033[0m', '\n')

    # checking the number of illuminated panels
    print('number of illuminated panels', res_df[res_df['IS_ILLUMINATED']==True].count()[0])
    print('it should be', int(selected_scenario['num_panels']*selected_scenario['illumination multiplier']), '\n')

    # checking the number of '048-F' size panels
    print('number of 048-F panels is', res_df[res_df['SIZE_CODE']=='048-F'].count()[0], '\n')

    # checking the total budget hasn't been exceeded
    print('client spend is: £' + str(res_df['PANEL_PRICE'].sum()))
    print('it should be less than: £' + str(selected_scenario['budget']), '\n')

    # checking the that minimum impacts has been exceeded
    print('total package impacts is:', str(int(res_df['IMPACTS'].sum())) + 'K')
    print('it should be around:', str(int(selected_scenario['impacts'])) + "K", '\n')
    #dd-cell

    ## fill this dictionary with the resulting dataframes
    result_dict= {}
    result_dict[SOLUTION]= res_df

    return result_dict
