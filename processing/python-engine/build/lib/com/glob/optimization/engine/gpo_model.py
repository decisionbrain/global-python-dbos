from datetime import datetime
from os import read
from os import name
from docplex.mp.dvar import Var
import pandas as pd
import re
import numpy as np
from docplex.mp.model import Model
from docplex.mp.relaxer import Relaxer
from collections import defaultdict

#---------------------------------------------------------------------------------------------------------
##########################################################################################################
#---------------------------------------------------------------------------------------------------------

class GlobalModel(Model):
    '''
    This class is a specialized version of the :class:`docplex.mp.model.Model` 
    class with useful non-standard modeling functions specific to Global 
    constraint modelling. (partially stolen from ed, cheers mate)
    '''
    def __init__(self, name=None, **kwargs):
        Model.__init__(self, name=name, **kwargs)
        self.decision_variables = []
        self.slack_variables = []
        self.var_counts = {}
        
    #-----------------------------------------------------------------------------------------------------
    #-----------------------------------------------------------------------------------------------------
    
    #--------------------------------------------------------------------
    # Functions specific to calculating 'Usage'
    #--------------------------------------------------------------------

    # Usage refers to the quantity of constraints of a given type (e.g. LE, GE, RNG) created within a certain level (e.g. Location or Panel Size)
    # E.g. if location spreadability constraints are created for 11 different locations (e.g. London, Yorkshire etc.), the 'usage' = 13

    # If inside brackets, replace with ''
    # E.g. a slack variable FeasVar_Location(london)_LE ----> FeasVar_Location_LE
    # Feeding this normalised name into _var_counts() below then allows all 'Location' slack variables to be counted and therefore, 'usage' to be determined

    @staticmethod
    def _normalise_name(str_input): # if inside brackets, replace with ''
       return re.sub(r'\(.*?\)', '', str_input)
    
    ###Creates 'usage' parameter for counting variable usages on different levels (e.g. Location)
    def _var_counts(self, var):
        # generating the normalised slack variable names and counts, for slack coefficients 
        normalised_name = self._normalise_name(var.name)
        if self.var_counts.get(normalised_name):
            self.var_counts[normalised_name] += 1
        else:
            self.var_counts[normalised_name] = 1
    
    #-----------------------------------------------------------------------------------------------------
    #-----------------------------------------------------------------------------------------------------

    #--------------------------------------------------------------------
    # Functions specific to Relaxer()
    #--------------------------------------------------------------------

    # converting user-defined weights to priorities for Relaxer
    def _weight_to_priority(self, str_input):
        weight = self.scenario['constraint_weights'][re.search("_(.*?)_",self._normalise_name(str_input)).group(1)]
        if weight == 1:
            priority = 'LOW'
        elif weight == 2:
            priority = 'MEDIUM'
        elif weight == 3:
            priority = 'HIGH'
        return priority
    
    # Normalising constraints before they are set, such that the Relaxer minimises 'normalised' constraint misses as opposed to 'raw' misses
    # this accounts for scale normalisation (dividing by rhs) and usage
    def _constraint_normalise(self, ct_lhs_o, ct_rhs_o, ct_expr=None, ct_usage=1):
        if ct_expr is None: # for non-range constraints, ct_lhs refers to the constraint expression and ct_rhs refers to the respective bound
            try:
                cf = 1 / (ct_rhs_o * ct_usage)
                ct_lhs, ct_rhs = ( x * cf for x in (ct_lhs_o, ct_rhs_o))
            except ZeroDivisionError:
                cf = 1 / ct_usage
                ct_lhs, ct_rhs = ( x / ct_usage for x in (ct_lhs_o, ct_rhs_o))
            return ct_lhs, ct_rhs, cf, ct_rhs_o
        else: # for range constraints, ct_lhs_o refers to lower bound and ct_rhs_o refers to upper bound
            try:
                norm_factor = 0.5 * (ct_lhs_o + ct_rhs_o)
                cf = 1 / (norm_factor * ct_usage)
                ct_lhs, ct_rhs, ct_expr = (x * cf for x in (ct_lhs_o, ct_rhs_o, ct_expr))
                ct_rhs_o = norm_factor # for storage in the slack dictionary
            except ZeroDivisionError:
                cf = 1 / ct_usage
                ct_lhs, ct_rhs, ct_expr = (x * cf for x in (ct_lhs_o, ct_rhs_o, ct_expr))
            return ct_lhs, ct_rhs, ct_expr, cf, ct_rhs_o
    
    #-----------------------------------------------------------------------------------------------------
    #-----------------------------------------------------------------------------------------------------

    #--------------------------------------------------------------------
    # Functions specific to slack variables
    #--------------------------------------------------------------------

    # CREATES SLACK VARIABLES FOR CONSTRAINTS
    def _slack_var(self, slack_name):
        slack_exists = self.get_var_by_name(slack_name)
        if slack_exists:
            return slack_exists  # return existing slack_var if possible
        slack_var = self.continuous_var(name=slack_name)

        self.slack_variables.append(slack_var)
        self._var_counts(slack_var)

        return slack_var
    
    #-----------------------------------------------------------------------------------------------------
    #-----------------------------------------------------------------------------------------------------

    #--------------------------------------------------------------------
    # Functions to add different constraint types (e.g. LE, GE, RNG etc.)
    #--------------------------------------------------------------------

    def add_le_constraint(self, ct_lhs, ct_rhs, ct_label, inc_slack=False, ct_usage=1):
        # creating a slack variable if inc_slack=True, else normalising constraints if inc_slack=False
        if inc_slack:
            slack_var = self._slack_var(f'FeasVar_{ct_label}_LE')
            ct_lhs = ct_lhs - slack_var
        else:
            ct_lhs, ct_rhs, cf, ct_rhs_o = self._constraint_normalise(ct_lhs, ct_rhs, ct_usage=ct_usage)

        # adding the constraint to the model
        ct = self.add_constraint(ct_lhs <= ct_rhs, ctname=f'Constraint_{ct_label}_LE')

        # setting the priority of constraints being added to the model for relaxation
        if not inc_slack:
            if 'Juxta' in ct_label:
                ct.set_mandatory()
            else:
                ct.coeff = cf
                ct.rhs_o = ct_rhs_o
                ct.usage = ct_usage
                ct.set_priority(self._weight_to_priority(ct.name))
                self._var_counts(ct)
    
    #-----------------------------------------------------------------------------------------------------
    
    def add_ge_constraint(self, ct_lhs, ct_rhs, ct_label, inc_slack=False, ct_usage=1):
        # creating a slack variable if inc_slack=True, else normalising constraints if inc_slack=False
        if inc_slack:
            slack_var = self._slack_var(f'FeasVar_{ct_label}_GE')
            ct_lhs = ct_lhs + slack_var
        else:
            ct_lhs, ct_rhs, cf, ct_rhs_o = self._constraint_normalise(ct_lhs, ct_rhs, ct_usage=ct_usage)

        # adding the constraint to the model
        ct = self.add_constraint(ct_lhs >= ct_rhs, ctname=f'Constraint_{ct_label}_GE')

        # setting the priority of constraints being added to the model for relaxation
        if not inc_slack:
            ct.coeff = cf
            ct.rhs_o = ct_rhs_o
            ct.usage = ct_usage
            ct.set_priority(self._weight_to_priority(ct.name))
            self._var_counts(ct)
    
    #-----------------------------------------------------------------------------------------------------
    
    def add_eq_constraint(self, ct_lhs, ct_rhs, ct_label):
        # adding the constraint to the model - no slack variables or relaxation as these are 'hard' constraints
        ct = self.add_constraint(ct_lhs == ct_rhs, ctname=f'Constraint_{ct_label}_EQ')
        if 'NumPanels' in ct_label:
            ct.set_mandatory()
    
    #-----------------------------------------------------------------------------------------------------

    def add_soft_eq_constraint(self, ct_lhs, ct_rhs, ct_label, inc_slack=True):
        # not currently used in the model
        self.add_le_constraint(ct_lhs, ct_rhs, ct_label, inc_slack=True)
        self.add_ge_constraint(ct_lhs, ct_rhs, ct_label, inc_slack=True)
    
    #-----------------------------------------------------------------------------------------------------

    def add_range_constraint(self, ct_lb, ct_expr, ct_ub, ct_label, inc_slack=False, ct_usage=1):
        if inc_slack:
            slack_var_le = self._slack_var(f'FeasVar_{ct_label}_RNG_LE')
            slack_var_ge = self._slack_var(f'FeasVar_{ct_label}_RNG_GE')
            #self.add_constraint(slack_var_le>=0, ctname=f'Positive_inforcement_{ct_label}_le')
            #self.add_constraint(slack_var_ge>=0, ctname=f'Positive_inforcement_{ct_label}_ge')
            ct_expr = ct_expr + slack_var_ge - slack_var_le
        else:
            ct_lb, ct_ub, ct_expr, cf, ct_rhs_o = self._constraint_normalise(ct_lb, ct_ub, ct_expr, ct_usage=ct_usage)
            
        ct = self.add_range(ct_lb, ct_expr, ct_ub, rng_name=f'Constraint_{ct_label}_RNG')

        if not inc_slack:
            ct.coeff = cf
            ct.rhs_o = ct_rhs_o
            ct.usage = ct_usage
            ct.set_priority(self._weight_to_priority(ct.name))
            self._var_counts(ct)

#---------------------------------------------------------------------------------------------------------
##########################################################################################################
#---------------------------------------------------------------------------------------------------------

class GPO_Model(GlobalModel):
    '''
    This is a specialsed class of the GlobalModel with attempt to solve the 
    gpo problem
    '''

    #------------------------------------------------
    # Initialising parameters and cleaning dataframes
    #------------------------------------------------

    def __init__(self, inputs, name=None, scenario=None, solve_method=1,  **kwargs):
        GlobalModel.__init__(self, name=name, **kwargs)
        
        # initialises
        self.df_panel_dictionary = {}
        self.scenario = scenario
        self.bin_var_dic = {}
        self.solve_method = solve_method
        self.inc_slack = False if self.solve_method in (4,5,6) else True

        # setting the relaxer mode
        if self.solve_method == 4:
            self.relax_mode = 'MinInf'
        elif self.solve_method == 5:
            self.relax_mode = 'MinSum'
        elif self.solve_method == 6:
            self.relax_mode = 'MinQuad'
        
        # loads panel df and cleans
        self.df_panel = inputs['Panels_-_Roadside_1']
        for i in ['LOCATION_TYPE_NAME','REGION_NAME','AREA_NAME','SUB_AREA_NAME']:
            self.df_panel[i] = self.df_panel[i].str.lower()
        self.df_panel = self.df_panel.set_index('PANEL_CODE')
        
        # loads loc df and cleans
        self.df_loc = inputs['Location_-_Roadside']
        self.df_loc['REGION_NAME'] = self.df_loc['REGION_NAME'].str.lower()
        self.df_loc = self.df_loc[1:]
        self.df_loc = self.df_loc.set_index('REGION_NAME')

        # loads close panel df and cleans
        self.df_close_panel = inputs['Close_Panels_v2']
        self.df_close_panel['REGION_NAME'] = self.df_close_panel['REGION_NAME'].str.lower()
        self.juxta = {'pairs' : self.df_close_panel[['PANEL_CODE_L','PANEL_CODE_R']]}

        # loads environment df and cleans 
        self.df_env = inputs['Environment_Types_-_Roadside']
        self.df_env['LOCATION_TYPE_NAME'] = self.df_env['LOCATION_TYPE_NAME'].str.lower()
        self.df_env = self.df_env.set_index('LOCATION_TYPE_NAME')

        # loads format df and cleans 
        self.df_size = inputs['Size_Types_-_Roadside']
        self.df_size = self.df_size.set_index('SIZE_CODE')

    '''removes unncessercy rows from df_panel based on scenario definition'''
    def clean_df_panel(self):
        if self.scenario['locations'] == 'focussed':
            self.df_panel = self.df_panel[self.df_panel['REGION_NAME'].isin(self.scenario['locations_ls'])]
        if self.scenario['size'] == 'focussed':
            self.df_panel = self.df_panel[self.df_panel['SIZE_CODE'].isin(self.scenario['size_ls'])]
        if self.scenario['hfss'] == True:
            self.df_panel = self.df_panel[self.df_panel['MIN_SCHOOL_DISTANCE'] >= 200]
        if self.scenario['blacklisted'] == False:
            self.df_panel = self.df_panel[self.df_panel['IsBlacklisted'] == 0]
        if self.scenario['local'] == False:
            self.df_panel = self.df_panel[self.df_panel['IsLongTerm'] == 0]
        
    '''cleans up spread data frames for loc/size/env etc to only include 
    required locations sizes etc and also change U/L bounds accordingly
    (U/L bounds currently set at 1.2x and 0.8x). can this be done quicker?'''
    def clean_spread_df(self):
        #location fd
        if self.scenario['locations'] == 'focussed':
            self.df_loc = self.df_loc.loc[self.scenario['locations_ls']]
            tot = self.df_loc['Ratio'].sum()
            for i in ['Ratio','Upper Bound','Lower Bound']:
                self.df_loc[i] = self.df_loc[i]/tot
        #size df
        if self.scenario['size'] == 'focussed':
            self.df_size = self.df_size.loc[self.scenario['size_ls']]
            tot = self.df_size['Ratio'].sum()
            for i in ['Ratio','Upper Bound','Lower Bound']:
                self.df_size[i] = self.df_size[i]/tot
        #env df
        if self.scenario['environment'] == 'focussed':
            self.df_env = self.df_env.loc[self.scenario['environment_ls']]
            tot = self.df_env['Ratio'].sum()
            for i in ['Ratio','Upper Bound','Lower Bound']:
                self.df_env[i] = self.df_env[i]/tot
                
        
    '''creates binary vairable (1 or 0) for all panels in the df. This will be 1 
    if we want to include the panel in the package and 0 if we do not. 
    also adds all of these to a dictionary so I can call these for later constraints'''
    def create_bin_variables(self):
        panels = list(self.df_panel.index.values)
        bin_var_dic = self.binary_var_dict(panels, name=[i for i in panels])
        self.bin_var_dic = bin_var_dic

    def total_panel_constraint(self):
        ## could change this to range constraint if needed
        tot = self.scenario['num_panels']
        expr = self.sum_vars(self.bin_var_dic)
        #self.add_constraint(sum(bin_var_dic.values()) == tot, ctname = 'num_of_panels')
        self.add_eq_constraint(expr, tot, 'NumPanels')
    
    '''adds lower and upper bounds for the impact weighting of the panels currently
    set at 0.9 and 1.1 respectively'''
    def adds_impact_weighting(self):
        ub = self.scenario['impact ratio upper threshold']*self.scenario['num_panels']
        lb = self.scenario['impact ratio lower threshold']*self.scenario['num_panels']
        iws = self.df_panel['IMPACT_WEIGHTING']
        keys1 = [self.get_var_by_name(i) for i in self.df_panel.index.values]
        #expr = self.sum(self.bin_var_dic[i]*iws[i] for i in self.bin_var_dic)
        expr = self.dot(keys1,iws)
        #self.add_le_constraint(expr, ub, 'ImpactWeighting', self.inc_slack)
        #self.add_ge_constraint(expr, lb, 'ImpactWeighting', self.inc_slack)
        self.add_range_constraint(lb, expr, ub, 'ImpactWeighting', self.inc_slack)

    '''This is just to convert to dictionary, testing purposes at the moment see below method'''
    def convert_to_dic(self):
        self.df_panel_dictionary = self.df_panel.to_dict()

    '''an alternative way to tackle adding methods without dataframes just raw lists and dictionarys.
    A lot less intuative and could be harder to debug in the future but maybe around 5% faster from 
    initial testing (is it worth it?)'''
    def adds_impact_weighting_numpy(self):
        ub = 1.01*self.scenario['num_panels']
        lb = 0.99*self.scenario['num_panels']
        req_col = self.df_panel_dictionary['IMPACT_WEIGHTING']
        keys1 = [self.get_var_by_name(i) for i in req_col.keys()]
        expr = self.dot(keys1,req_col.values)
        #self.add_range_constraint(lb, expr, ub, 'ImpactWeighting', self.inc_slack)



    '''adds illumination constraint at the moment this is fixed number but can be changed 
    to ratio or call from a df etc if required'''
    def illum_constraint(self):
        df_illum = self.df_panel[self.df_panel['IS_ILLUMINATED']==True]
        illum_pan_ls = list(df_illum.index.values)
        expr = self.sum(self.bin_var_dic[i] for i in illum_pan_ls)
        illum_req = round(self.scenario['num_panels']*self.scenario['illumination multiplier'])
        #self.add_constraint(expr == illum_req, ctname = 'illum')
        self.add_le_constraint(expr, illum_req, 'Illumination', self.inc_slack)
        self.add_ge_constraint(expr, illum_req, 'Illumination', self.inc_slack)


    def illum_constraint_speedy(self):
        illum_dict = self.df_panel_dictionary['IS_ILLUMINATED']
        illum_panels = {k:v for (k,v) in illum_dict.items() if v is True}
        #expr = sum(self.bin_var_dic[i] for i in illum_pan_ls)
        expr = self.sum(self.get_var_by_name(i) for i in illum_panels)
        illum_req = round(self.scenario['num_panels']*self.scenario['illumination multiplier'])
        #self.add_constraint(expr == illum_req, ctname = 'illum')
        self.add_le_constraint(expr, illum_req, 'Illumination', self.inc_slack)
        self.add_ge_constraint(expr, illum_req, 'Illumination', self.inc_slack)
        #self.add_range_constraint(illum_req*0.9,expr,illum_req*1.1,'Illumination')

    # CODE CHANGED TO CALCULATE USAGE PRIOR TO ADDING CONSTRAINTS #
    def loc_bounds(self): 
        # setting location | bounds dictionary
        self.df_loc = self.df_loc[round(self.df_loc['Upper Bound'] * self.scenario['num_panels']) != 0]
        self.df_loc['REGION_NAME'] = self.df_loc.index
        self.df_loc = self.df_loc.loc[self.df_loc['REGION_NAME'].isin(self.df_panel['REGION_NAME'].unique())]
        del self.df_loc['REGION_NAME']
        loc_bounds = self.df_loc.apply(list).to_dict()
        ct_usage = len(loc_bounds['Ratio']) # counts the number of regions in the proposed package, before creation

        # setting region | panels dictionary
        self.df_panel['PANEL_CODE'] = self.df_panel.index
        loc_groups = self.df_panel.groupby('REGION_NAME')['PANEL_CODE'].apply(list).to_dict()
        del self.df_panel['PANEL_CODE']

        for region in list(self.df_loc.index.values):
            ub = round(self.scenario['num_panels']*loc_bounds['Upper Bound'][region])
            lb = round(self.scenario['num_panels']*loc_bounds['Lower Bound'][region])
            expr = self.sum([self.get_var_by_name(i) for i in loc_groups[region]])
            self.add_range_constraint(lb, expr, ub, f'Location({region})', self.inc_slack, ct_usage)

    
    '''adds spread constraint with upper and lower bounds for getting 
    good spread over locations. currently upper and lower bounds are set at 
    20% above and below (i.e. 1.2x and 0.8x ratio)'''
    def loc_ul_bounds(self):
        ## loops through just df loc at the moment how can this work for df_size/env?
        ref_ls = list(self.df_loc.index.values)
        df_panel_grouped = self.df_panel.groupby(['REGION_NAME'])
        for row in ref_ls:
            try:
                df_panel_ref = df_panel_grouped.get_group(row)
            except:
                print(f'Unable to add constraint for {row} as no available panels in location')
                continue
            lb = round(self.scenario['num_panels']*self.df_loc['Lower Bound'][row])
            ub = round(self.scenario['num_panels']*self.df_loc['Upper Bound'][row])
            keys1 = [self.get_var_by_name(i) for i in df_panel_ref.index.values]
            expr = self.sum(keys1)
            if ub != 0:
                self.add_range_constraint(lb, expr, ub, f'Location({row})', self.inc_slack)
                #self.add_le_constraint(expr, ub, self.inc_slack, f'Location({row})')
                #self.add_ge_constraint(expr, lb, self.inc_slack, f'Location({row})')
            else:
                print(f'{row} UB = 0 so no constraint added')

    '''adds spread constraint with upper and lower bounds for getting 
    good spread over enivronemtns. currently upper and lower bounds are set at 
    20% above and below (i.e. 1.2x and 0.8x ratio)'''
    def env_ul_bounds(self):
        ref_ls = list(self.df_env.index.values)
        df_panel_grouped = self.df_panel.groupby(['LOCATION_TYPE_NAME'])
        for row in ref_ls:
            try:
                df_panel_ref = df_panel_grouped.get_group(row)
            except:
                print(f'Unable to add constraint for {row} as no available panels with requirement')
                continue
            panel_ref_ls = list(df_panel_ref.index.values)
            lb = round(self.scenario['num_panels']*self.df_env['Lower Bound'][row])
            ub = round(self.scenario['num_panels']*self.df_env['Upper Bound'][row])
            expr = sum(self.bin_var_dic[i] for i in panel_ref_ls)
            if ub != 0:
                #self.add_range(lb, expr, ub, rng_name = f'{row}')
                self.add_le_constraint(expr, ub, f'Env({row})', self.inc_slack)
                self.add_ge_constraint(expr, lb, f'Env({row})', self.inc_slack)
            else:
                print(f'{row} UB = 0 so no constraint added')

    '''adds spread constraint with upper and lower bounds for getting 
    good spread over size. currently upper and lower bounds are set at 
    20% above and below (i.e. 1.2x and 0.8x ratio)'''
    def size_ul_bounds(self):
        ref_ls = list(self.df_size.index.values)
        df_panel_grouped = self.df_panel.groupby(['SIZE_CODE'])
        for row in ref_ls:
            try:
                df_panel_ref = df_panel_grouped.get_group(row)
            except:
                print(f'Unable to add constraint for {row} as no available panels with requirement')
                continue
            panel_ref_ls = list(df_panel_ref.index.values)
            lb = round(self.scenario['num_panels']*self.df_size['Lower Bound'][row])
            ub = round(self.scenario['num_panels']*self.df_size['Upper Bound'][row])
            expr = sum(self.bin_var_dic[i] for i in panel_ref_ls)
            if ub != 0:
                #self.add_range(lb, expr, ub, rng_name = f'{row}')
                self.add_le_constraint(expr, ub, f'Size({row})', self.inc_slack)
                self.add_ge_constraint(expr, lb, f'Size({row})', self.inc_slack)
            else:
                print(f'{row} UB = 0 so no constraint added')

    '''adds constraint to not include panels which are close (< 50 metres) to each other
       currently only considers close 048-F Roadside panels, but will be expanded in future'''
    def remove_close_panels(self):
        self.ct_cp_list = []
        for juxta_pair in self.juxta['pairs'].to_numpy():
            panel_l, panel_r = juxta_pair
            if isinstance(self.get_var_by_name(panel_l), Var) and isinstance(self.get_var_by_name(panel_r), Var):
                ct_name = f'Juxta({panel_l},{panel_r})'
                lhs = self.sum(self.get_var_by_name(i) for i in juxta_pair)
                self.add_le_constraint(lhs, 1, ct_name)

    ''' adds close panel constraints in batches. quicker than above.'''
    def remove_close_panel_v2(self):
        exprs1 = []
        ct_names = []
        pan1 = list(self.df_close_panel['PANEL_CODE_L'])
        pan2 = list(self.df_close_panel['PANEL_CODE_R'])
        for i in range(len(pan1)):
            x = self.get_var_by_name(pan1[i])
            y = self.get_var_by_name(pan2[i])
            if x and y:
                exp = self.sum([x,y])
                ct_names.append(f'Juxta({x},{y})')
                exprs1.append(exp<=1)

        self.add_constraints(exprs1, ct_names)
    
    '''adds constraint for client budget'''
    def budget(self):
        keys1 = [self.get_var_by_name(i) for i in self.df_panel.index.values]
        vals = self.df_panel['PANEL_PRICE']
        expr = self.dot(keys1,vals)
        ub = 1.01*self.scenario['budget']
        lb = 0.99*self.scenario['budget']
        self.add_range_constraint(lb, expr, ub, 'Budget', self.inc_slack)
    
    '''adds soft equality constraint for client impacts
       client perspective - 'we want to achieve a certain amount of impacts'
       global perspective - 'we want to preserve our inventory, such that we don't over-allocate impacts and keep the maximum possible impacts available for future orders'
    '''
    def impacts(self):
        keys1 = [self.get_var_by_name(i) for i in self.df_panel.index.values]
        vals = self.df_panel['IMPACTS']
        expr = self.dot(keys1,vals)
        ub = 1.01*self.scenario['impacts']
        lb = 0.99*self.scenario['impacts']
        self.add_range_constraint(lb, expr, ub, 'Impacts', self.inc_slack)
        #self.add_le_constraint(expr, ub, 'Impacts', self.inc_slack)
        #self.add_ge_constraint(expr, lb, 'Impacts', self.inc_slack)
    
    '''builds the model'''
    def build_model(self):
        t1 = datetime.now()
        self.clean_df_panel()
        self.clean_spread_df()
        
        t3 = datetime.now()
        self.create_bin_variables()

        t3_1 = datetime.now()
        self.total_panel_constraint()
        
        #t3_5 = datetime.now()
        #t3_6 = datetime.now()
        #self.convert_to_dic()

        t4 = datetime.now()
        self.adds_impact_weighting()
        #self.adds_impact_weighting_numpy()
        
        t5 = datetime.now()
        self.illum_constraint()
        #self.illum_constraint_speedy()
        
        t6 = datetime.now()
        #self.loc_ul_bounds()
        self.loc_bounds()
        #self.size_ul_bounds()
        #self.env_ul_bounds()
        
        t7 = datetime.now()
        #self.remove_close_panels() # currently only considers close 048-F Roadside panels, but will be expanded in future
        self.remove_close_panel_v2()

        t8 = datetime.now()
        self.budget()
        
        t9 = datetime.now()
        self.impacts()
        
        t10 = datetime.now()
        if self.inc_slack: # if we use slack, go to the slack_min_objective_function()
            if self.solve_method == 1:
                self.slack_min_objective_function()
            if self.solve_method == 2:
                self.lexo_objective_function()
            if self.solve_method == 3:
                self.lexo_objective_function_v2()
        else: # if we don't use slack, we have a simple constraint satisfaction problem, with no objective function (?)
            self.set_objective_sense('min')
            self.set_objective_expr(0)

        t11 = datetime.now() 

        print('\n',f'Model Build Timings:')     
        print(f'took {t3-t1} to clean dfs')
        print(f'took {t3_1-t3} to create binary vars')
        print(f'took {t4-t3_1} to add total panel num constraint')
        print(f'took {t5-t4} to add impact weighting constraint')
        print(f'took {t6-t5} to add illum constraint')
        print(f'took {t7-t6} to add spread constraints')
        print(f'took {t8-t7} to add remove close panels constraint')
        print(f'took {t9-t8} to add budget constraint')
        print(f'took {t10-t9} to add impacts constraint')
        print(f'took {t11-t10} to build objective function')
        print(f'total build time: {t11-t1}','\n')
    
    #-----------------------------------------------------------------------------------------------------
    
    def slack_min_objective_function(self):
        
        # creating the slack variable dictionary for objective function computation and data output
        self.slack_dict = self.create_slack_dict(self.slack_variables)

        # setting the objective function expression as a KPI to see in the output
        slack_min_expr = self.dot(self.slack_dict['variable_names'], self.slack_dict['coefficients'])
        self.add_kpi(slack_min_expr, publish_name='kpi1')

        # minimising the sum of the dot products between the slack variables and their respective coefficients
        self.set_objective_sense('min')
        self.set_objective_expr(slack_min_expr)

    #-----------------------------------------------------------------------------------------------------

    def create_slack_dict(self, var_list):

        # creating the slack variable dictionary for objective function computation and data output
        # this could probably be split into individual functions for Regular and Relaxer for tidyness!

        self.slack_dict = defaultdict(list)
        for slack_var in var_list:
            normalised_name = self._normalise_name(slack_var.name)

            # getting the rhs of each constraint to normalise the slack variable value
            ct_name = re.sub('FeasVar','Constraint', slack_var.name)
            if self.inc_slack:
                if 'RNG' in ct_name:
                    ct_name = ct_name[:-3] if self.inc_slack else ct_name
                    lb = self.get_constraint_by_name(ct_name).lb
                    ub = self.get_constraint_by_name(ct_name).ub
                    slack_rhs = 0.5*(lb+ub)
                else:
                    slack_rhs = self.get_constraint_by_name(ct_name).rhs.get_constant()
            else:
                slack_rhs = self.get_constraint_by_name(ct_name).rhs_o # getting 'old' rhs for normalised Relaxer constraints
            
            slack_usage = self.var_counts[normalised_name] if self.inc_slack else self.get_constraint_by_name(ct_name).usage

            # getting the relevant slack weight from the scenario dictionary
            regex = "_(.*?)_"
            weight_group = re.search(regex,normalised_name).group(1)
            slack_weight = self.scenario['constraint_weights'][weight_group]

            # calculating the slack coefficient
            if self.inc_slack:
                try:
                    slack_coefficient = slack_weight / (slack_usage * slack_rhs)
                except ZeroDivisionError:
                    slack_coefficient = slack_weight / slack_usage
            else:
                slack_coefficient = self.get_constraint_by_name(ct_name).coeff
            
            # appending slack data to slack_dict
            self.slack_dict['variable_names'].append(slack_var)
            self.slack_dict['normalised_names'].append(normalised_name)
            self.slack_dict['weight'].append(slack_weight)
            self.slack_dict['usages'].append(slack_usage)
            self.slack_dict['rhs'].append(slack_rhs)
            self.slack_dict['coefficients'].append(slack_coefficient)
        
        self.slack_df = pd.DataFrame(data=self.slack_dict)

        return self.slack_dict
        
    #-----------------------------------------------------------------------------------------------------
    
    def lexo_objective_function(self):

        # Create objective function
        self.slack_dict = defaultdict(list)
        self.lexo_slack_dict = defaultdict(dict)
        exprs1 = []
        priorities1 = []
        obj_names = []
        tols = []
        for slack_var in self.slack_variables:
            slack_normalised_name = self._normalise_name(slack_var.name)
            slack_usage = self.var_counts[slack_normalised_name]

            # getting the rhs of each constraint to normalise the slack variable value
            ct_name = re.sub('FeasVar','Constraint', slack_var.name)
            if ct_name[-7:-2] == '_RNG_':
                ct_name = ct_name[:-3]
                lb = self.get_constraint_by_name(ct_name).lb
                ub = self.get_constraint_by_name(ct_name).ub
                slack_rhs = 0.5*(lb+ub)
            else:
                slack_rhs = self.get_constraint_by_name(ct_name).rhs.get_constant()

            # getting the relevant slack weight from the scenario dictionary
            regex = "_(.*?)_"
            weight_group = re.search(regex,slack_normalised_name).group(1)
            slack_weight = self.scenario['constraint_weights'][weight_group]

            # calculating the slack coefficient
            try:
                slack_coefficient = slack_weight / (slack_usage * slack_rhs)
            except ZeroDivisionError:
                slack_coefficient = slack_weight / slack_usage

            # appending slack data to slack_dict
            self.slack_dict['variable_names'].append(slack_var)
            self.slack_dict['normalised_names'].append(slack_normalised_name)
            self.slack_dict['weight'].append(slack_weight)
            self.slack_dict['usages'].append(slack_usage)
            self.slack_dict['rhs'].append(slack_rhs)
            self.slack_dict['coefficients'].append(slack_coefficient)

            # creating lexogrpahical slack dict to improve comp speed
            try:
                self.lexo_slack_dict[slack_weight]['variable_names'].append(slack_var)
                self.lexo_slack_dict[slack_weight]['coefficients'].append(slack_coefficient)
            except:
                self.lexo_slack_dict[slack_weight]['variable_names'] = [slack_var]
                self.lexo_slack_dict[slack_weight]['coefficients'] = [slack_coefficient]

        # creates expresions, priorities and names for objective functions
        for i in self.lexo_slack_dict:
            exp = self.dot(self.lexo_slack_dict[i]['variable_names'], self.lexo_slack_dict[i]['coefficients'])
            exprs1.append(exp)
            priorities1.append(i)
            obj_names.append(f'priority_{i}')
            tols.append(0.05)
        self.slack_df = pd.DataFrame(data=self.slack_dict)

        # creates objective functions and add priorities 
        self.set_multi_objective(sense='min', exprs=exprs1, priorities=priorities1, names=obj_names, reltols=tols)
        self.add_kpi(self.sum(i for i in exprs1),publish_name='kpi1')
    
    
    #-----------------------------------------------------------------------------------------------------

    def lexo_objective_function_v2(self):
        # Create objective function
        self.slack_dict = defaultdict(list)
        self.lexo_slack_dict = defaultdict(dict)
        for slack_var in self.slack_variables:
            slack_normalised_name = self._normalise_name(slack_var.name)
            slack_usage = self.var_counts[slack_normalised_name]

            # getting the rhs of each constraint to normalise the slack variable value
            ct_name = re.sub('FeasVar','Constraint', slack_var.name)
            if ct_name[-7:-2] == '_RNG_':
                ct_name = ct_name[:-3]
                lb = self.get_constraint_by_name(ct_name).lb
                ub = self.get_constraint_by_name(ct_name).ub
                slack_rhs = 0.5*(lb+ub)
            else:
                slack_rhs = self.get_constraint_by_name(ct_name).rhs.get_constant()

            # getting the relevant slack weight from the scenario dictionary
            regex = "_(.*?)_"
            weight_group = re.search(regex,slack_normalised_name).group(1)
            slack_weight = self.scenario['constraint_weights'][weight_group]

            # calculating the slack coefficient
            try:
                slack_coefficient = slack_weight / (slack_usage * slack_rhs)
            except ZeroDivisionError:
                slack_coefficient = slack_weight / slack_usage

            # appending slack data to slack_dict
            self.slack_dict['variable_names'].append(slack_var)
            self.slack_dict['normalised_names'].append(slack_normalised_name)
            self.slack_dict['weight'].append(slack_weight)
            self.slack_dict['usages'].append(slack_usage)
            self.slack_dict['rhs'].append(slack_rhs)
            self.slack_dict['coefficients'].append(slack_coefficient)

            # creating lexogrpahical slack dict to improve comp speed
            try:
                self.lexo_slack_dict[slack_weight]['variable_names'].append(slack_var)
                self.lexo_slack_dict[slack_weight]['coefficients'].append(slack_coefficient)
            except:
                self.lexo_slack_dict[slack_weight]['variable_names'] = [slack_var]
                self.lexo_slack_dict[slack_weight]['coefficients'] = [slack_coefficient]
        self.slack_df = pd.DataFrame(data=self.slack_dict)

        # setting the objective function expression as a KPI to see in the output
        slack_min_expr = self.dot(self.slack_dict['variable_names'], self.slack_dict['coefficients'])
        self.add_kpi(slack_min_expr, publish_name='kpi1')

    #-----------------------------------------------------------------------------------------------------

    def lexo_v2_solve(self):
        dict_keys = list(self.lexo_slack_dict.keys())
        # adds high priority constraints (weighting == 3) and solves
        if 3 in dict_keys:
            expr_high = self.dot(self.lexo_slack_dict[3]['variable_names'], self.lexo_slack_dict[3]['coefficients'])
            self.minimize(expr_high)
            sol1 = self.solve(log_output=True)
            self.report_kpis()
            max_slack = sol1.get_value(expr_high)
            self.add_constraint(expr_high <= max_slack * 1.05)
            for index, dvar in enumerate(self.solution.iter_variables()):
                if "FeasVar" in dvar.to_string():
                    print (dvar.to_string(), '=', self.solution[dvar]) 
        # adds constraint that high priority must be within 5% of minimized solve
        # solves for mid priority
        if 2 in dict_keys:
            expr_mid = self.dot(self.lexo_slack_dict[2]['variable_names'], self.lexo_slack_dict[2]['coefficients'])
            self.minimize(expr_mid)
            sol2 = self.solve(log_output=True)
            self.report_kpis()
            #print(sol2.get_all_values())
            max_slack_mid = sol2.get_value(expr_mid)
            self.add_constraint(expr_mid <= max_slack_mid * 1.05)
            for index, dvar in enumerate(self.solution.iter_variables()):
                if "FeasVar" in dvar.to_string():
                    print (dvar.to_string(), '=', self.solution[dvar]) 
        # adds constraint that mid priority must be within 5% of minimized solve
        # solves for low priority
        if 1 in dict_keys:
            expr_low = self.dot(self.lexo_slack_dict[1]['variable_names'], self.lexo_slack_dict[1]['coefficients'])
            self.minimize(expr_low)
            sol3 = self.solve(log_output=True)
            self.report_kpis()
            #print(sol3.get_all_values())
            for index, dvar in enumerate(self.solution.iter_variables()):
                if "FeasVar" in dvar.to_string():
                    print (dvar.to_string(), '=', self.solution[dvar])
        
        # putting the slack variable values and priorities in slack dictionary
        self.slack_dict['normalised_values'] = [self.solution[j] * self.slack_dict['coefficients'][i] / self.slack_dict['weight'][i] for i,j in enumerate(self.slack_dict['variable_names'])]
        self.slack_dict['values'] = [self.solution[i] for i in self.slack_dict['variable_names']]
        self.slack_dict['priority'] = [self._weight_to_priority(i.name) for i in self.slack_dict['variable_names']] # adding priority here for comparison with relaxer

    #-----------------------------------------------------------------------------------------------------
    
    '''solves model'''
    def solve_model(self, time_lim = 45, target = 0):

        # solving the model
        self.set_time_limit(time_lim)
        self.parameters.mip.tolerances.mipgap.set(target)
        sol = self.solve(log_output=False)

        if sol and self.inc_slack:
            # putting the slack variable values in slack dictionary
            self.slack_dict['normalised_values'] = [self.solution[j] * self.slack_dict['coefficients'][i] / self.slack_dict['weight'][i] for i,j in enumerate(self.slack_dict['variable_names'])]
            self.slack_dict['values'] = [self.solution[i] for i in self.slack_dict['variable_names']]
            self.slack_dict['priority'] = [self._weight_to_priority(i.name) for i in self.slack_dict['variable_names']] # adding priority here for comparison with relaxer

            # printing the slack variable values
            print(5*'\n', 'Slack Variable Values:')
            for index, dvar in enumerate(self.solution.iter_variables()):
                if "FeasVar" in dvar.to_string():
                    print (dvar.to_string(), '=', self.solution[dvar])
            
            # reporting the equation and value of the objective function
            print('\n', 'Objective Function Value (Slack Minimisation):')
            self.report_kpis()
        else:
            self.relaxer()
    
    #-----------------------------------------------------------------------------------------------------

    def relaxer(self):
            print("Not solved - starting Relaxer...")
            self.rx = Relaxer()
            self.rx.relax(self, relax_mode=self.relax_mode, log_output = False)

            # getting relaxed constraints and using create_slack_dict to determine their respective 'coefficients'
            r_vars = list(self.rx.relaxations().keys())
            self.slack_dict = self.create_slack_dict(r_vars)
            self.slack_dict['normalised_values'] = [abs(self.rx.get_relaxation(i)) for i in self.slack_dict['variable_names']]
            self.slack_dict['values'] =  [nv / c for nv,c in zip(self.slack_dict['normalised_values'], self.slack_dict['coefficients'])]
            self.slack_dict['variable_names'] = [i.name for i in self.slack_dict['variable_names']] # making names more meaningful
            self.slack_dict['priority'] = [self._weight_to_priority(i) for i in self.slack_dict['variable_names']] # adding in constraint priorities

            # getting the value of the 'relaxed' objective function
            self.r_qual = self.sum(i[0] * i[1] for i in zip(self.slack_dict['normalised_values'], self.slack_dict['weight']))
            self.add_kpi(self.r_qual, publish_name='kpi1')

            # re-defining the objective function
            """ self.set_objective_sense('min')
            self.set_objective_expr(self.r_qual)
            self.solve_model() """

            # printing the main 'relaxer' information
            print(5*'\n', 'Relaxed Solution Quality:', self.r_qual)

            print('\n', 'Relaxed Constraints:')
            for ct, val in self.rx.iter_relaxations():
                print(ct.name, '=', val)